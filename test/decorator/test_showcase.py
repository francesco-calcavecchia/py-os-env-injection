import os
from typing import Optional, Tuple, Callable

import pytest

from os_env_injection import inject_os_env, OSEnvInjectionError, Injection, inject_var


@inject_os_env(
    injections=[
        Injection(var_name="url", os_env_key="OS_ENV_URL"),
        Injection(var_name="user", os_env_key="OS_ENV_USER"),
        Injection(var_name="password", os_env_key="OS_ENV_PASSWORD"),
        Injection(var_name="subdomain", os_env_key="OS_ENV_SUBDOMAIN", is_required=False),
    ]
)
def f_functional(
    url: str,
    user: str,
    password: str,
    subdomain: Optional[str],
) -> Tuple[str, str, str, str]:
    return url, user, password, subdomain


def f_imperative(
    url: str | None = None, user: str | None = None, password: str | None = None, subdomain: str | None = None
) -> Tuple[str, str, str, str]:
    url = inject_var(var_value=url, os_env_key="OS_ENV_URL")
    user = inject_var(var_value=user, os_env_key="OS_ENV_USER")
    password = inject_var(var_value=password, os_env_key="OS_ENV_PASSWORD")
    subdomain = inject_var(var_value=subdomain, os_env_key="OS_ENV_SUBDOMAIN", is_required=False)
    return url, user, password, subdomain


@pytest.mark.parametrize("f", [f_functional, f_imperative])
def test_if_no_input_then_return_os_env(f: Callable):
    assert f() == ("https://www.mydomain.com", "user name", "secret password", None)


@pytest.mark.parametrize("f", [f_functional, f_imperative])
def test_if_missing_required_os_env_then_raise_exception(f: Callable):
    del os.environ["OS_ENV_USER"]
    with pytest.raises(OSEnvInjectionError):
        f()


@pytest.mark.parametrize("f", [f_functional, f_imperative])
def test_if_only_one_value_provided_as_arg_then_return_it(f: Callable):
    assert f("use this instead") == ("use this instead", "user name", "secret password", None)


@pytest.mark.parametrize("f", [f_functional, f_imperative])
def test_if_only_one_value_provided_as_kwarg_then_return_it(f: Callable):
    assert f(password="use this instead") == ("https://www.mydomain.com", "user name", "use this instead", None)


@pytest.mark.parametrize("f", [f_functional, f_imperative])
def test_if_optional_value_passed_then_return_it(f: Callable):
    assert f(subdomain="use this instead") == (
        "https://www.mydomain.com",
        "user name",
        "secret password",
        "use this instead",
    )


@pytest.mark.parametrize("f", [f_functional, f_imperative])
def test_if_optional_value_is_in_os_env_then_return_it(f: Callable):
    os.environ["OS_ENV_SUBDOMAIN"] = "subdomain"
    assert f() == ("https://www.mydomain.com", "user name", "secret password", "subdomain")
    del os.environ["OS_ENV_SUBDOMAIN"]


@pytest.mark.parametrize("f", [f_functional, f_imperative])
def test_if_optional_value_is_passed_and_in_os_env_then_return_passed_value(f: Callable):
    os.environ["OS_ENV_SUBDOMAIN"] = "subdomain"
    assert f(subdomain="use this instead") == (
        "https://www.mydomain.com",
        "user name",
        "secret password",
        "use this instead",
    )
    del os.environ["OS_ENV_SUBDOMAIN"]


@pytest.fixture(autouse=True)
def setup():
    os.environ["OS_ENV_URL"] = "https://www.mydomain.com"
    os.environ["OS_ENV_USER"] = "user name"
    os.environ["OS_ENV_PASSWORD"] = "secret password"
    yield None
    for v in "OS_ENV_URL", "OS_ENV_USER", "OS_ENV_PASSWORD":
        if v in os.environ.keys():
            del os.environ[v]
